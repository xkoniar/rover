#include "geometry_msgs/Twist.h"
#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/Joy.h"
#include "std_msgs/Header.h"

std::size_t R1 = 6; 
std::size_t L1 = 4;
std::size_t R2 = 7;
std::size_t L2 = 5;

std::size_t JY = 0;
std::size_t JX = 1;

geometry_msgs::Twist TWIST_MSG;
std_msgs::Float64 YAW_MSG;
std_msgs::Float64 PITCH_MSG;

ros::Publisher TWIST_PUB;
ros::Publisher YAW_PUB;
ros::Publisher PITCH_PUB;

void joystick_cb(const sensor_msgs::JoyConstPtr& msg_ptr) {
    double left_v = 0.;
    double right_v = 0.;

    if (msg_ptr->buttons[R1]) {
        left_v = 2.;
    } else if (msg_ptr->buttons[L1]) {
        left_v = -2.;
    }

    if (msg_ptr->buttons[R2]) {
        right_v = 2.;
    } else if (msg_ptr->buttons[L2]) {
        right_v = -2.;
    }


    ROS_INFO_STREAM(left_v << "," << right_v);

    TWIST_MSG.linear.x = 2* (left_v + right_v) / 2;
    TWIST_MSG.angular.z = left_v - right_v;

    YAW_MSG.data = -4*msg_ptr->axes[JY] - 1.5*msg_ptr->axes[JX];
    PITCH_MSG.data = -4*msg_ptr->axes[JY] + 1.5*msg_ptr->axes[JX];
}

void timer_cb(const ros::TimerEvent&) {
    TWIST_PUB.publish(TWIST_MSG);
    YAW_PUB.publish(YAW_MSG);
    PITCH_PUB.publish(PITCH_MSG);
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "rover_control");

    ros::NodeHandle nh{"~"};

    TWIST_PUB = nh.advertise<geometry_msgs::Twist>("/pca/mobile_base_controller/cmd_vel", 1);
    YAW_PUB = nh.advertise<std_msgs::Float64>("/pca/gimbal_yaw_controller/command", 1);
    PITCH_PUB = nh.advertise<std_msgs::Float64>("/pca/gimbal_pitch_controller/command", 1);
    auto joy_sub = nh.subscribe("/joy", 1, joystick_cb);

    ros::Timer timer = nh.createTimer(ros::Duration(0.1), timer_cb);

    ros::spin();

    return 0;
}
