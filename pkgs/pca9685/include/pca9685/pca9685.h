/**
 * Copyright 2017 - 2018 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */

/*
 * Heavily inspired by adafruit python code, due to that reasons I also include their license for the original python
 * code, last seen on: https://github.com/adafruit/Adafruit_Python_PCA9685/blob/master/Adafruit_PCA9685/PCA9685.py
 * Adafruit license is commented by //.
 */

// Author: Tony DiCola
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*
 * Now follows a code that used the original Adafruit code as inspiration, but is a new piece of code by itself.
 */

#include "pca9685/config.h"
#include "pca9685/i2c.h"
#include "pca9685/pca9685_interfaces.h"
#include "pca9685/util.h"
#include "pca9685/util/physics.h"
#include <hardware_interface/robot_hw.h>
#include <pluginlib/class_loader.h>

namespace pca9685
{

template < typename IO = StandardIO >
class PCA9685Hardware : public hardware_interface::RobotHW
{

      public:
        using ChannelHandlerPtr = boost::shared_ptr< IChannelHandler >;

        explicit PCA9685Hardware( const PCA9685Config& config )
          : plugin_loader_( "pca9685", "pca9685::IChannelHandler" )
          , i2c_com_( config.i2c.device_path, config.i2c.slave_addr )

        {

                // Initialize all channels specified by user
                ROS_INFO( "initializing channels: " );

                for ( const ChannelConfig& chconfig : config.channels ) {
                        ROS_INFO_STREAM( chconfig.name );

                        ChannelHandlerPtr ptr{plugin_loader_.createInstance( chconfig.plugin_name )};
                        ROS_ASSERT( ptr );

                        RegistrationRequest req = ptr->setup( chconfig.name );

                        for ( hardware_interface::JointStateHandle handle : req.joint_state_handles ) {
                                interfaces_.jnt_state_interface.registerHandle( handle );
                        }

                        for ( hardware_interface::JointHandle handle : req.joint_pos_handles ) {
                                interfaces_.jnt_pos_interface.registerHandle( handle );
                        }

                        for ( hardware_interface::JointHandle handle : req.joint_vel_handles ) {
                                interfaces_.jnt_vel_interface.registerHandle( handle );
                        }

                        channel_handlers_.push_back( ptr );
                }

                registerInterface( &interfaces_.jnt_state_interface );
                registerInterface( &interfaces_.jnt_vel_interface );
                registerInterface( &interfaces_.jnt_pos_interface );

                std::set< uint8_t > active_channels;
                bool                shutdown = false;
                for ( ChannelHandlerPtr& channel : channel_handlers_ ) {
                        if ( active_channels.find( channel->getChannelID() ) != active_channels.end() ) {
                                ROS_WARN_STREAM( "Multiple channels of ID: " << channel->getChannelID() );
                                shutdown = true;
                        }
                        active_channels.insert( channel->getChannelID() );
                }
                if ( shutdown ) {
                        ros::shutdown();
                }

                if ( !initI2CCom() ) {
                        ROS_WARN_STREAM( "Initialization of i2c during node construction failed, stopping" );
                        ros::shutdown();
                        return;
                }
        }

        void tick( const ros::Time& time, const ros::Duration& period )
        {
                for ( ChannelHandlerPtr& channel : channel_handlers_ ) {
                        ROS_ASSERT( channel );
                        channel->tick( time, period );
                }
        }

        void write( const ros::Time& time, const ros::Duration& period ) override
        {
                for ( ChannelHandlerPtr& channel : channel_handlers_ ) {
                        ROS_ASSERT( channel );
                        uint8_t channel_number = channel->getChannelID();

                        this->setPWM( channel_number, channel->getPulse( time, period ) );
                }
        }

      private:
        static constexpr uint8_t  DRIVER_BITS        = 0x0C;  // number of bits in driver resolution
        static constexpr uint8_t  MODE1              = 0x00;  // Mode  register  1
        static constexpr uint8_t  MODE2              = 0x01;  // Mode  register  2
        static constexpr uint8_t  CHANNEL0_ON_L      = 0x06;
        static constexpr uint8_t  CHANNEL0_ON_H      = 0x07;
        static constexpr uint8_t  CHANNEL0_OFF_L     = 0x08;  // CHANNEL0 output and brightness control byte 2
        static constexpr uint8_t  CHANNEL0_OFF_H     = 0x09;  // CHANNEL0 output and brightness control byte 3
        static constexpr uint8_t  ALL_CHANNEL_ON_L   = 0xFA;
        static constexpr uint8_t  ALL_CHANNEL_ON_H   = 0xFB;
        static constexpr uint8_t  ALL_CHANNEL_OFF_L  = 0xFC;
        static constexpr uint8_t  ALL_CHANNEL_OFF_H  = 0xFD;
        static constexpr uint8_t  CHANNEL_MULTIPLYER = 4;  // For the other 15 channels
        static constexpr uint8_t  PWM_FREQ   = 60;    // actual PCA9685 frequence we need for servos, not really general
        static constexpr uint8_t  PRE_SCALE  = 0xFE;  // prescaler for output frequency
        static constexpr unsigned CLOCK_FREQ = 25000000.0;  // 25MHz default osc clock
        static constexpr uint8_t  RESTART    = 0x80;
        static constexpr uint8_t  SLEEP      = 0x10;
        static constexpr uint8_t  ALLCALL    = 0x01;
        static constexpr uint8_t  OUTDRV     = 0x04;

        pluginlib::ClassLoader< IChannelHandler > plugin_loader_;
        PCA9685Interfaces                         interfaces_;
        I2CCom< IO >                              i2c_com_;
        std::vector< ChannelHandlerPtr >          channel_handlers_;

        bool initI2CCom()
        {

                if ( !setAllPWM( Pulse{0} ) ) {
                        ROS_WARN_STREAM( "Was not able to set all pwm channels to 0 on PCA9685" );
                        return false;
                }

                if ( !i2c_com_.writeByte( MODE2, OUTDRV ) ) {
                        ROS_WARN_STREAM( "Was not able to set MODE2 to OUTDRV on PCA9685" );
                        return false;
                }
                if ( !i2c_com_.writeByte( MODE1, ALLCALL ) ) {
                        ROS_WARN_STREAM( "Was not able to set MODE1 to ALLCALL on PCA9685" );
                        return false;
                }

                // There should be a short delay in inicialization of the PCA9685
                ros::Duration( 0.005 ).sleep();

                I2CRead mode1_read = i2c_com_.readByte( MODE1 );
                if ( !mode1_read.success ) {
                        ROS_WARN_STREAM( "Did not receive a MODE1 byte from PCA9685" );
                        return false;
                }
                auto mode1 = uint8_t( mode1_read.byte & ~SLEEP );
                if ( !i2c_com_.writeByte( MODE1, mode1 ) ) {
                        ROS_WARN_STREAM( "Was not able to set MODE1 of PCA9685" );
                        return false;
                }

                ros::Duration( 0.005 ).sleep();
                return setPWMFreq( PWM_FREQ );
        }

        bool setPWMFreq( double freq )
        {
                const double pre_base = ( CLOCK_FREQ / pow( 2, DRIVER_BITS ) ) / freq - 1;
                ROS_ASSERT( pre_base > 0 );

                const auto prescale = uint8_t( pre_base );
                ROS_ASSERT( pre_base < std::numeric_limits< uint8_t >::max() );

                ROS_INFO_STREAM( "Setting prescale for PCA9685: " << int( prescale ) );

                I2CRead mode_read = i2c_com_.readByte( MODE1 );
                if ( !mode_read.success ) {
                        ROS_WARN_STREAM( "Did not receive a MODE1 byte from PCA9685" );
                        return false;
                }

                const uint8_t oldmode = mode_read.byte;
                const auto    mode    = uint8_t( ( oldmode & 0x7F ) | SLEEP );  // activate sleep bit

                if ( !i2c_com_.writeByte( MODE1, mode ) ) {
                        ROS_WARN_STREAM( "Was not able to set MODE1 with sleep bit on PCA9685" );
                        return false;
                }
                if ( !i2c_com_.writeByte( PRE_SCALE, prescale ) ) {
                        ROS_WARN_STREAM( "Was not able to set PRE_SCALE on PCA9685" );
                        return false;
                }
                if ( !i2c_com_.writeByte( MODE1, oldmode ) ) {
                        ROS_WARN_STREAM( "Was not able to set MODE1 without sleep bit on PCA9685" );
                        return false;
                }

                ros::Duration( 0.005 ).sleep();

                if ( !i2c_com_.writeByte( MODE1, oldmode | RESTART ) ) {
                        ROS_WARN_STREAM( "Was not able to set MODE1 with restart bit on PCA9685" );
                        return false;
                }
                return true;
        }

        uint16_t pwmToBits( Pulse pulse )
        {
                const double period    = 1. / PWM_FREQ;
                const double s_per_bit = period / pow( 2, DRIVER_BITS );

                // TODO(squirrel): review this madness

                auto float_val = double( pulse / s_per_bit );

                ROS_ASSERT( float_val < std::numeric_limits< uint16_t >::max() );

                return uint16_t( float_val );
        }

        bool setPWM( uint8_t channel_id, Pulse pulse )
        {

                ROS_DEBUG_STREAM_THROTTLE( 5, "Setting channel " << int( channel_id ) << " to pulse: " << pulse );

                uint16_t value = pwmToBits( pulse );
                ROS_DEBUG_STREAM_THROTTLE( 5, "value to be sent: " << value );

                auto offset = uint8_t( CHANNEL_MULTIPLYER * channel_id );

                return setChannel( uint8_t( CHANNEL0_ON_L + offset ),
                                   uint8_t( CHANNEL0_ON_H + offset ),
                                   uint8_t( CHANNEL0_OFF_L + offset ),
                                   uint8_t( CHANNEL0_OFF_H + offset ),
                                   value );
        }

        bool setAllPWM( Pulse pulse )
        {
                uint16_t value = pwmToBits( pulse );
                ROS_DEBUG_STREAM_THROTTLE( 5, "value to be sent: " << value );

                return setChannel( ALL_CHANNEL_ON_L, ALL_CHANNEL_ON_H, ALL_CHANNEL_OFF_L, ALL_CHANNEL_OFF_H, value );
        }

        bool setChannel( uint8_t on_l, uint8_t on_h, uint8_t off_l, uint8_t off_h, uint16_t value )
        {
                if ( !i2c_com_.writeByte( on_l, 0 ) ) {
                        ROS_WARN_STREAM( "Was not able to sent byte to PCA9685, reg: " << int( on_l ) );
                        return false;
                }
                if ( !i2c_com_.writeByte( on_h, 0 ) ) {
                        ROS_WARN_STREAM( "Was not able to sent byte to PCA9685, reg: " << int( on_h ) );
                        return false;
                }
                if ( !i2c_com_.writeByte( off_l, uint8_t( value & 0xFF ) ) ) {
                        ROS_WARN_STREAM( "Was not able to sent byte to PCA9685, reg: " << int( off_l ) );
                        return false;
                }
                if ( !i2c_com_.writeByte( off_h, uint8_t( value >> 8 ) ) ) {
                        ROS_WARN_STREAM( "Was not able to sent byte to PCA9685, reg: " << int( off_h ) );
                        return false;
                }

                return true;
        }
};  // namespace pca9685
}  // namespace pca9685
