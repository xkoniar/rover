/**
 * Copyright 2017 - 2018 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */
#include <algorithm>
#include <ros/ros.h>

#pragma once

namespace pca9685
{


template < typename T >
T clamp( T val, T val_min, T val_max )
{
  return std::min< T >( val_max, std::max< T >( val_min, val ) );
}

template < typename SourceType, typename TargetType >
TargetType
linearInterp( SourceType val, SourceType from_min, SourceType from_max, TargetType to_min, TargetType to_max )
{
  ROS_ASSERT( double( from_max - from_min ) > 0 );
  TargetType res{double( val - from_min ) * double( to_max - to_min ) / double( from_max - from_min )};
  return res + to_min;
}

}  // namespace pca9685
