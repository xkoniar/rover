
#include <fcntl.h>  // flags for open
#include <sys/ioctl.h>
#include <unistd.h>  // dup()

#pragma once

namespace pca9685
{
class StandardIO
{
public:
  // TODO(squirrel): think about 'pragma schpin function_collection', a mark for code checker, that this class should just be a
  // collection of static methods, that internally calls one function, have same name as that one function and same type
  // signature.
  static int open( const char* pathname, int flags )
  {
    return ::open( pathname, flags );
  }
  static int ioctl( int fd, unsigned long request, unsigned int argp )
  {
    return ::ioctl( fd, request, argp );
  }
  static int dup( int fd )
  {
    return fcntl(fd, F_DUPFD_CLOEXEC);
  }
  static ssize_t read( int fildes, void* buf, size_t nbyte )
  {
    return ::read( fildes, buf, nbyte );
  }
  static ssize_t write( int fildes, const void* buf, size_t nbyte )
  {
    return ::write( fildes, buf, nbyte );
  }
  static int close( int fd )
  {
    return ::close( fd );
  }
};
} // namespace pca9685
