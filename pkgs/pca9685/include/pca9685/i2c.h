/**
 * Copyright 2017 - 2018 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */
#include "pca9685/standard_io.h"
#include "pca9685/util.h"
#include <bitset>
#include <cerrno>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <linux/i2c-dev.h>
#include <ros/assert.h>
#include <ros/ros.h>
#include <string>

#pragma once

namespace pca9685
{

struct I2CRead
{
  bool    success;
  uint8_t byte;
};

template < typename IO = StandardIO >
class I2CCom
{
public:
  using FD = int;

  I2CCom( const std::string& device_name, unsigned address )
  {

    ROS_INFO_STREAM( "Opening device " << device_name << " with address: " << address );

    fd_ = IO::open( device_name.c_str(), O_RDWR );

    if ( fd_ == -1 )
    {
      ROS_WARN_STREAM( "Failed to open the I2C device: " << std::strerror( errno ) );
      ros::shutdown();
    }

    ROS_DEBUG_STREAM( "Opened file descriptor: " << fd_ );

    if ( !setAddress( address ) )
    {
      ROS_WARN_STREAM( "Failed to set the I2C addr during creation, shutting down" );
      ros::shutdown();
    }
  }

  I2CCom( const I2CCom& other )
    : fd_( IO::dup( other.fd_ ) )
  {
  }

  I2CCom& operator=( const I2CCom& other )
  {
    I2CCom copy( other );
    std::swap( fd_, copy.fd_ );
    return *this;
  }

  bool setAddress( unsigned address )
  {
    if ( fd_ == 0 )
    {
      ROS_WARN_STREAM( "I2C device is not initialized, can't set address." );
      return false;
    }

    if ( address == 0 )
    {
      ROS_WARN_STREAM( "Can't set addr to 0" );
      return false;
    }

    int ioctl_res = IO::ioctl( fd_, I2C_SLAVE, address );

    if ( ioctl_res == -1 )
    {
      ROS_WARN_STREAM( "Error when setting the ioctl: " << std::strerror( errno ) );
      return false;
    }

    return true;
  }

  bool writeByte( uint8_t register_addr, uint8_t val )
  {
    ROS_DEBUG_STREAM( "Writing to file descriptor: " << fd_ );

    if ( fd_ == 0 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "I2C device is not initialized, can't write byte." );
      return false;
    }

    uint8_t bytes[2];
    bytes[0] = register_addr;
    bytes[1] = val;

    ssize_t write_res = IO::write( fd_, bytes, 2 );

    if ( write_res == -1 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "Error with writing to stream in errno: " << std::strerror( errno ) );
      return false;
    }

    if ( write_res != 2 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "Write to I2C device did not take 2 bytes, written: " << write_res );
      return false;
    }

    ROS_DEBUG_STREAM( "Written " << val << " to register: " << register_addr );
    return true;
  }

  I2CRead readByte( uint8_t register_addr )
  {
    ROS_DEBUG_STREAM( "Opened file descriptor: " << fd_ );

    if ( fd_ == 0 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "I2C device is not initialized, can't read byte." );
      return I2CRead{false, 0};
    }

    uint8_t write_msg[1];
    write_msg[0] = register_addr;

    ssize_t write_res = IO::write( fd_, write_msg, 1 );

    if ( write_res == -1 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "Error happend during the write to an I2C device: " << std::strerror( errno ) );
      return I2CRead{false, 0};
    }

    if ( write_res != 1 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "Write, during the read of an byte, to i2c device did not take 1 byte." );
      return I2CRead{false, 0};
    }

    uint8_t read_msg[1];

    ssize_t read_res = IO::read( fd_, read_msg, 1 );

    if ( read_res == -1 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "Error happend during the write to an I2C device: " << std::strerror( errno ) );
      return I2CRead{false, 0};
    }

    if ( read_res != 1 )
    {
      ROS_WARN_STREAM_THROTTLE( 10, "Read from i2c did not returned 1 byte." );
      return I2CRead{false, 0};
    }

    return I2CRead{true, read_msg[0]};
  }

  virtual ~I2CCom()
  {
    if ( fd_ == 0 )
    {
      return;
    }
    int close_res = IO::close( fd_ );

    if ( close_res == -1 )
    {
      ROS_WARN_STREAM( "Error happend, when closing the i2c device: " << std::strerror( errno ) );
    }
  }

private:
  FD fd_;
};
}  // namespace pca9685
