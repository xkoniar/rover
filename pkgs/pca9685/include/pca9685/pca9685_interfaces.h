/**
 * Copyright 2017 - 2018 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */

#include "pca9685/util/physics.h"
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>

#pragma once

namespace pca9685
{
struct PCA9685Interfaces
{
        hardware_interface::JointStateInterface    jnt_state_interface;
        hardware_interface::VelocityJointInterface jnt_vel_interface;
        hardware_interface::PositionJointInterface jnt_pos_interface;
};

struct RegistrationRequest
{
        std::vector< hardware_interface::JointStateHandle > joint_state_handles;
        std::vector< hardware_interface::JointHandle >      joint_vel_handles;
        std::vector< hardware_interface::JointHandle >      joint_pos_handles;
};

class IChannelHandler
{
      public:
        virtual RegistrationRequest setup( std::string name )                                = 0;
        virtual void                tick( const ros::Time&, const ros::Duration& )           = 0;
        virtual uint8_t             getChannelID() const                                     = 0;
        virtual Pulse               getPulse( const ros::Time&, const ros::Duration& ) const = 0;
        virtual ~IChannelHandler()                                                           = default;
};
}  // namespace pca9685
