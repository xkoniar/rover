/**
 * Copyright 2017 - 2018 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */

#include <dynamic_reconfigure/server.h>
#include <pca9685/PWMMotorConfig.h>
#include <pca9685/pca9685_interfaces.h>
#include <pca9685/util.h>
#include <pca9685/util/physics.h>
#include <ros/ros.h>

namespace pca9685
{
class PWMConfig
{
      public:
        MicroPulse      pulse_min    = 0;
        MicroPulse      pulse_center = 0;
        MicroPulse      pulse_max    = 0;
        AngularVelocity vel_min      = 0;
        AngularVelocity vel_max      = 0;
        bool            reverse      = false;
        uint8_t         channel      = 0;

        PWMConfig( ros::NodeHandle& node_handle )
          : node_handle_( node_handle )
          , server_( node_handle_ )
        {
                server_.setCallback( boost::bind( &PWMConfig::setConfig, this, _1, _2 ) );
        }
        PWMConfig( PWMConfig&& other )      = delete;
        PWMConfig( const PWMConfig& other ) = delete;
        PWMConfig& operator=( PWMConfig&& other ) = delete;
        PWMConfig& operator=( const PWMConfig& other ) = delete;

        void setConfig( PWMMotorConfig& config, uint32_t /*level*/ )
        {
                pulse_min    = MicroPulse( config.pulse_min );
                pulse_center = MicroPulse( config.pulse_center );
                pulse_max    = MicroPulse( config.pulse_max );
                vel_min      = AngularVelocity( config.vel_min );
                vel_max      = AngularVelocity( config.vel_max );
                reverse      = config.reverse;
                channel      = uint8_t( config.channel );
                ROS_ASSERT( config.channel < std::numeric_limits< uint8_t >::max() );
        }

      private:
        ros::NodeHandle&                              node_handle_;
        dynamic_reconfigure::Server< PWMMotorConfig > server_;
};

class PWMMotor final : public IChannelHandler
{
        double                       position_;
        double                       goal_velocity_;
        double                       velocity_;
        double                       effort_;
        std::unique_ptr< PWMConfig > config_;

        RegistrationRequest setup( std::string name )
        {
                ros::NodeHandle node_handle{"~/channels/" + name};
                config_.reset( new PWMConfig( node_handle ) );

                RegistrationRequest req;

                hardware_interface::JointStateHandle state_handle( name, &position_, &velocity_, &effort_ );
                req.joint_state_handles.push_back( state_handle );

                // TODO: passing state_handle here may be a bad idea, it will move in memory
                hardware_interface::JointHandle pos_handle( state_handle, &goal_velocity_ );
                req.joint_vel_handles.push_back( pos_handle );

                return req;
        }

        uint8_t getChannelID() const
        {
                ROS_ASSERT( config_ );
                return config_->channel;
        }

        Pulse getPulse( const ros::Time&, const ros::Duration& ) const
        {
                ROS_ASSERT( config_ );
                AngularVelocity ang_vel{goal_velocity_};

                ang_vel = clamp( ang_vel, config_->vel_min, config_->vel_max );

                float rat;

                if ( ang_vel > AngularVelocity( 0. ) ) {
                        rat = linearInterp( ang_vel, AngularVelocity( 0. ), config_->vel_max, 0., 1. );
                } else {
                        rat = linearInterp( ang_vel, config_->vel_min, AngularVelocity( 0. ), -1., 0. );
                }

                if ( config_->reverse ) {
                        rat = -rat;
                }

                if ( rat > 0. ) {
                        return linearInterp( rat, 0.f, 1.f, config_->pulse_center, config_->pulse_max )
                            .toRatio< NeutralRatio >();
                } else {
                        return linearInterp( rat, -1.f, 0.f, config_->pulse_min, config_->pulse_center )
                            .toRatio< NeutralRatio >();
                }
        }

        void tick( const ros::Time&, const ros::Duration& durat )
        {
                position_ += goal_velocity_ * durat.toSec();
        }
};
}  // namespace pca9685
