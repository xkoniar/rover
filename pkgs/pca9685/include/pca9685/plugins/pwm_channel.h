/**
 * Copyright 2017 - 2018 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */

#include "std_msgs/UInt32.h"
#include <cmath>
#include <pca9685/pca9685_interfaces.h>
#include <pca9685/util.h>
#include <pca9685/util/physics.h>
#include <ros/ros.h>

#pragma once

namespace pca9685
{
class PWMChannel final : public IChannelHandler
{
        int             channel_ = 0;
        MicroPulse      pulse_   = 0;
        ros::Subscriber sub_;

        RegistrationRequest setup( std::string name )
        {
                ros::NodeHandle node_handle{"~/channels/" + name};

                if ( !node_handle.getParam( "channel", channel_ ) ) {
                        ROS_ERROR( "Failed to load channel param" );
                        ros::shutdown();
                }

                if ( channel_ < 0 || channel_ > 16 ) {
                        ROS_ERROR( "Bad channel vlaue" );
                        ros::shutdown();
                }

                sub_ = node_handle.subscribe( "channel", 10, &PWMChannel::channelCallback, this );

                return RegistrationRequest{};
        }

        void channelCallback( const std_msgs::UInt32ConstPtr& msg_ptr )
        {
                pulse_ = MicroPulse( msg_ptr->data );
        }

        uint8_t getChannelID() const
        {
                return channel_;
        }
        Pulse getPulse( const ros::Time&, const ros::Duration& ) const
        {
                return pulse_.toRatio< NeutralRatio >();
        }
        void tick( const ros::Time&, const ros::Duration& )
        {
        }
};
}  // namespace pca9685
