#include <ros/console.h>
#include <string>
#include <vector>
#include "pca9685/util.h"

#pragma once

namespace pca9685 {

struct ChannelConfig {
    std::string name;
    std::string plugin_name;
};

struct I2CConfig {
    std::string device_path;
    unsigned slave_addr{};
};

struct PCA9685Config {
    double frequency = 60;
    std::vector<ChannelConfig> channels{};
    I2CConfig i2c;
};

const inline PCA9685Config loadStaticConfig(
    const ros::NodeHandle& node_handle) {
    PCA9685Config config;
    bool critical_problem = false;

    if (!node_handle.getParam("frequency", config.frequency)) {
        ROS_WARN_STREAM("Parameter frequency not specified, setting to default:"
                        << config.frequency);
        node_handle.setParam("frequency", config.frequency);
    }

    if (!node_handle.getParam("i2c/device", config.i2c.device_path)) {
        ROS_FATAL_STREAM("Parameter i2c/device not configured");
        critical_problem = true;
    }

    int signed_slave_addr;
    if (!node_handle.getParam("i2c/slave", signed_slave_addr)) {
        ROS_FATAL_STREAM("Parameter i2c/slave not configured");
        critical_problem = true;
    } else if (signed_slave_addr < 0) {
        ROS_FATAL_STREAM("Parameter i2c/slave has to be > 0");
        critical_problem = true;
    } else {
        config.i2c.slave_addr = unsigned(signed_slave_addr);
    }

    XmlRpc::XmlRpcValue channel_list;
    
    if (!node_handle.getParam("channels", channel_list)) {
        ROS_FATAL_STREAM("No channels configured for PCA9685");
        critical_problem = true;
    }
    for (const auto& channel : channel_list) {
        ChannelConfig chconfig;

        chconfig.name = channel.first;
        if (!node_handle.getParam("channels/" + chconfig.name + "/type",
                                  chconfig.plugin_name)) {
            ROS_FATAL_STREAM(
                "Failed to load 'type' param of channel: " << chconfig.name);
            critical_problem = true;
        }

        config.channels.push_back(chconfig);
    }

    if (critical_problem) {
        ROS_FATAL_STREAM(
            " There was a critical problem in loading the configuration, "
            "shutting down");
        ros::shutdown();
    }

    return config;
}
}  // namespace pca9685
