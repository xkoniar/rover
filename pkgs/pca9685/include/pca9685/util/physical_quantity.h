/**
 * Copyright 2017 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */

#include <cmath>
#include <ostream>
#include <ratio>

#pragma once

/** @todo Make the mechanism recognize 'strings' for each tag, 's', 'm', 'g' and so on  */

namespace pca9685
{

/** Base structure for other tags of physical quantity.
 *
 * Physical quantity tag is datatype that marks the semantical meaning of specific physical quantity.
 * That datatype is preferably struct, that inherits 'PhysicalQuantityTag'. This is verified inside the PhysicalQuantity
 * class.
 *
 * For a physical quantity that should represent 'distance', this means that the tag should be 'struct DistanceTag :
 * PhysicalQuantityTag'
 *
 */
struct PhysicalQuantityTag
{
};

using Atto         = std::atto;
using Centi        = std::centi;
using Deca         = std::deca;
using Deci         = std::deci;
using Exa          = std::exa;
using Femto        = std::femto;
using Giga         = std::giga;
using Hecto        = std::hecto;
using Kilo         = std::kilo;
using Mega         = std::mega;
using Micro        = std::micro;
using Milli        = std::milli;
using Nano         = std::nano;
using Peta         = std::peta;
using Pico         = std::pico;
using Tera         = std::tera;
using NeutralRatio = std::ratio< 1, 1 >;

/** Structure used to detect if type is std::ratio
 *
 * The is_ratio<T>::value is true if the type T is std::ratio<N,D> with some parameters N,D.
 * The is_ratio<T>::value is false otherwise.
 *
 * This is used internally by PhysicalQuantity.
 *
 * @param T is type which is analyzed.
 */
template < typename T >
struct is_ratio
{
        template < typename U >
        struct test : std::false_type
        {
        };

        template < std::intmax_t Num, std::intmax_t Denom >
        struct test< std::ratio< Num, Denom > > : std::true_type
        {
        };

        static constexpr bool value = test< T >::value;
};

/** Class representing generic physical quantity.
 *
 * This class is used to generate a type for specific physical quantity.
 * Idea is that most of the physical quantites behave same, but wi are missing strong typedefs in C++.
 * Hence the best solution to have strong types representing the quantities, is to create a class that can be used to
 * create specific physical quantities.
 *
 * That is done not on level of class instances, but on datatype level with templates.
 *
 * The 'Tag' datatypes is what distinguishes physical quantities between each other on datatype level.
 * The standart set of operators is defined in a way that allows only physical quantities with same Tag to interact with
 * each other. The type itself is however not used internally.
 *
 * Idea is that you create your custom tag, such as 'struct DistanceTag : PhysicalQuantityTag {}' and create a distance
 * datatype: 'using Distance = PhysicalQuantity< NeutralRatio, DistanceTag>;'. Multiple types representing physical
 * quantities can be created and as long as they have different tags, they won't interact with each other by default.
 *
 * Set of operations that this class allows should be standart set required, so this 'feels' like using standart double
 * datatype. It is possible and advised to create operators between multiple types of physical quantities. Thus creating
 * safe system of math for physics in the code.
 *
 * The 'Ratio' typename is used to represent physical quantity, scaled to different value. The scale itself is stored
 * with a usage of std::ratio<N,D> template. This template also provided ratios for several known metric prefixes, which
 * are provided with this class.
 *
 * Datatypes with different ratios, but same tags, are convertible between each other. Either automatically through copy
 * constructor or with the 'toRatio<R>()' template method. However operators are not made to interact between different
 * ratios, as the conversion can be costly in case two different ratios interact extensively.
 *
 * @param Ratio template param that represents scale of actual physical quantity.
 * @param Tag template param that specifies the semantical meaning of the physical quantity.
 *
 * Both parameters for this class are verified with static assert. It is expected that 'Ratio' parameter is std::ratio<>
 * template and that 'Tag' parameter is base of PhysicalQuantityTag.
 *
 */
template < typename Ratio, typename Tag >
class PhysicalQuantity final
{
        static_assert( is_ratio< Ratio >::value,
                       "The Ratio provided to PhysicalQuantity should be std::ratio template" );
        static_assert( std::is_base_of< PhysicalQuantityTag, Tag >::value,
                       "The Tag provided to PhysicalQuantity should inherit from the PhysicalQuantityTag" );
        double value_;

      public:
        /* Default constructor used to create a physical quantity from double value */
        PhysicalQuantity( double val )
          : value_( val )
        {
        }

        /* Constructo that allows conversion between physical quantities with same tag, but different ratio. The
         * internall value is correctly recalculated */
        template < typename OtherRatio >
        explicit PhysicalQuantity( const PhysicalQuantity< OtherRatio, Tag >& other )
          : value_( 0 )
        {
                double scale = double( OtherRatio::num * Ratio::den ) / double( OtherRatio::den * Ratio::num );
                value_       = other.get() * scale;
        }

        PhysicalQuantity& operator=( double val )
        {
                value_ = val;
        }

        /* Method to manually convert from actual ratio to specified ratio. */
        template < typename TargetRatio >
        PhysicalQuantity< TargetRatio, Tag > toRatio() const
        {
                return PhysicalQuantity< TargetRatio, Tag >( *this );
        }

        /* Const reference to the internal double value */
        const double& get() const
        {
                return value_;
        }

        /* This makes it possible to manually convert the quantity to double, just exposes the internal value. */
        explicit operator double() const
        {
                return double( this->get() );
        }

        PhysicalQuantity& operator+=( const PhysicalQuantity& other )
        {
                value_ += other.get();
                return *this;
        }
        PhysicalQuantity& operator-=( const PhysicalQuantity& other )
        {
                value_ -= other.get();
                return *this;
        }
        PhysicalQuantity& operator/=( const double val )
        {
                value_ /= val;
                return *this;
        }
        PhysicalQuantity& operator*=( const double val )
        {
                value_ *= val;
                return *this;
        }
};

template < typename Ratio, typename Tag >
inline double operator/( const PhysicalQuantity< Ratio, Tag >& lhs, const PhysicalQuantity< Ratio, Tag >& rhs )
{
        return lhs.get() / rhs.get();
}

template < typename Ratio, typename Tag >
inline PhysicalQuantity< Ratio, Tag > operator+( const PhysicalQuantity< Ratio, Tag >& lhs,
                                                 const PhysicalQuantity< Ratio, Tag >& rhs )
{
        PhysicalQuantity< Ratio, Tag > res( lhs.get() );
        res += rhs;
        return res;
}

template < typename Ratio, typename Tag >
inline PhysicalQuantity< Ratio, Tag > operator-( const PhysicalQuantity< Ratio, Tag >& lhs,
                                                 const PhysicalQuantity< Ratio, Tag >& rhs )
{
        PhysicalQuantity< Ratio, Tag > res( lhs.get() );
        res -= rhs;
        return res;
}

template < typename Ratio, typename Tag >
inline PhysicalQuantity< Ratio, Tag > operator-( const PhysicalQuantity< Ratio, Tag >& val )
{
        return PhysicalQuantity< Ratio, Tag >{-val.get()};
}

template < typename Ratio, typename Tag >
inline bool operator==( const PhysicalQuantity< Ratio, Tag >& lhs, const PhysicalQuantity< Ratio, Tag >& rhs )
{
        return lhs.get() == rhs.get();
}

template < typename Ratio, typename Tag >
inline bool operator<( const PhysicalQuantity< Ratio, Tag >& lhs, const PhysicalQuantity< Ratio, Tag >& rhs )
{
        return lhs.get() < rhs.get();
}

template < typename Ratio, typename Tag >
PhysicalQuantity< Ratio, Tag > operator*( const PhysicalQuantity< Ratio, Tag > pq, const double val )
{
        PhysicalQuantity< Ratio, Tag > res( pq.get() );
        res *= val;
        return res;
}

template < typename Ratio, typename Tag >
PhysicalQuantity< Ratio, Tag > operator/( const PhysicalQuantity< Ratio, Tag > pq, const double val )
{
        PhysicalQuantity< Ratio, Tag > res( pq.get() );
        res /= val;
        return res;
}

template < typename Ratio, typename Tag >
PhysicalQuantity< Ratio, Tag > abs( PhysicalQuantity< Ratio, Tag > a )
{
        return PhysicalQuantity< Ratio, Tag >( std::abs( a.get() ) );
}

template < typename Ratio, typename Tag >
double cos( const PhysicalQuantity< Ratio, Tag > u )
{
        return std::cos( u.get() );
}

template < typename Ratio, typename Tag >
double sin( const PhysicalQuantity< Ratio, Tag > u )
{
        return std::sin( u.get() );
}

template < typename Ratio, typename Tag >
PhysicalQuantity< Ratio, Tag > max( const PhysicalQuantity< Ratio, Tag >& lh, const PhysicalQuantity< Ratio, Tag >& rh )
{
        return PhysicalQuantity< Ratio, Tag >( std::max( lh.get(), rh.get() ) );
}

template < typename Ratio, typename Tag >
PhysicalQuantity< Ratio, Tag > min( const PhysicalQuantity< Ratio, Tag >& lh, const PhysicalQuantity< Ratio, Tag >& rh )
{
        return PhysicalQuantity< Ratio, Tag >( std::min( lh.get(), rh.get() ) );
}

/* @todo print also actual ratio and tag */
template < typename Ratio, typename Tag >
std::ostream& operator<<( std::ostream& os, const PhysicalQuantity< Ratio, Tag >& id )
{
        os << id.get();
        return os;
}

//---------------------------------------------------------------------------

template < typename Ratio, typename Tag >
inline bool operator!=( const PhysicalQuantity< Ratio, Tag >& lhs, const PhysicalQuantity< Ratio, Tag >& rhs )
{
        return !operator==( lhs, rhs );
}
template < typename Ratio, typename Tag >
inline bool operator>( const PhysicalQuantity< Ratio, Tag >& lhs, const PhysicalQuantity< Ratio, Tag >& rhs )
{
        return operator<( rhs, lhs );
}
template < typename Ratio, typename Tag >
inline bool operator<=( const PhysicalQuantity< Ratio, Tag >& lhs, const PhysicalQuantity< Ratio, Tag >& rhs )
{
        return !operator>( lhs, rhs );
}
template < typename Ratio, typename Tag >
inline bool operator>=( const PhysicalQuantity< Ratio, Tag >& lhs, const PhysicalQuantity< Ratio, Tag >& rhs )
{
        return !operator<( lhs, rhs );
}
//---------------------------------------------------------------------------

template < typename Ratio, typename Tag >
PhysicalQuantity< Ratio, Tag > operator*( const double val, const PhysicalQuantity< Ratio, Tag > pq )
{
        return pq * val;
}
template < typename Ratio, typename Tag >
inline PhysicalQuantity< Ratio, Tag > operator/( const double val, const PhysicalQuantity< Ratio, Tag >& pq )
{
        return PhysicalQuantity< Ratio, Tag >{val / pq.get()};
}
}  // namespace pca9685
