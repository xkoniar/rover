
#include "pca9685/util/physical_quantity.h"
#include <ros/ros.h>

#pragma once

namespace pca9685
{

struct PulseTag : PhysicalQuantityTag
{
};
using Pulse      = PhysicalQuantity< NeutralRatio, PulseTag >;
using MiliPulse  = PhysicalQuantity< Milli, PulseTag >;
using MicroPulse = PhysicalQuantity< Micro, PulseTag >;

struct AngleTag : PhysicalQuantityTag
{
};
using Angle = PhysicalQuantity< NeutralRatio, AngleTag >;

struct AngularVelocityTag : PhysicalQuantityTag
{
};
using AngularVelocity = PhysicalQuantity< NeutralRatio, AngularVelocityTag >;

struct AngularDistanceTag : PhysicalQuantityTag
{
};
using AngularDistance = PhysicalQuantity< NeutralRatio, AngularDistanceTag >;

inline AngularDistance operator*( const ros::Duration& lh, const AngularVelocity& rh )
{
  return AngularDistance{lh.toSec() * rh.get()};
}
} // namespace pca9685
