#include "pca9685/util/physics.h"
#include <gtest/gtest.h>
#include <ros/ros.h>

using namespace pca9685;

TEST( Physics, operators )
{
  ros::Duration   dur{2};
  AngularVelocity vel{2};

  AngularDistance dist = dur * vel;
  ASSERT_EQ( dist.get(), 4 );
}

int main( int argc, char** argv )
{
  ::testing::InitGoogleTest( &argc, argv );
  return RUN_ALL_TESTS();
}
