#include "pca9685/util/physical_quantity.h"
#include <gtest/gtest.h>

using namespace pca9685;

class TestTag : PhysicalQuantityTag
{
};

template < typename Ratio >
using PQ = PhysicalQuantity< Ratio, TestTag >;

using N = PQ< NeutralRatio >;

TEST( PhysicalQuantity, basic )
{
  N val{1};
  N half{0.5};

  EXPECT_EQ( val.get(), 1 );
  EXPECT_EQ( double( val ), 1 );
  EXPECT_EQ( N{val}.operator+=( half ).get(), 1.5 );
  EXPECT_EQ( N{val}.operator-=( half ).get(), 0.5 );
  EXPECT_EQ( N{val}.operator/=( 0.5 ).get(), 2 );
  EXPECT_EQ( N{val}.operator*=( 2 ).get(), 2 );
  EXPECT_EQ( operator/( val, half ), 2 );
  EXPECT_EQ( operator+( val, half ).get(), 1.5 );
  EXPECT_EQ( operator-( val, half ).get(), 0.5 );
  EXPECT_EQ( operator-( val ).get(), -1 );
  EXPECT_EQ( operator==( val, half ), false );
  EXPECT_EQ( operator<( val, half ), false );
  EXPECT_EQ( operator*( val, 2 ).get(), 2 );
  EXPECT_EQ( operator/( val, 0.5 ).get(), 2 );
  EXPECT_EQ( abs( -val ).get(), 1 );
  EXPECT_EQ( cos( val ), std::cos( val.get() ) );
  EXPECT_EQ( sin( val ), std::sin( val.get() ) );
  EXPECT_EQ( max( val, half ).get(), 1 );
  EXPECT_EQ( min( val, half ).get(), 0.5 );
  EXPECT_EQ( operator!=( val, half ), true );
  EXPECT_EQ( operator>( val, half ), true );
  EXPECT_EQ( operator<=( val, val ), true );
  EXPECT_EQ( operator<=( val, half ), false );
  EXPECT_EQ( operator>=( val, val ), true );
  EXPECT_EQ( operator>=( half, val ), false );
  EXPECT_EQ( operator*( 2, val ).get(), 2 );
  EXPECT_EQ( operator/( 1, half ).get(), 2 );
  std::stringstream ss;
  ss << val;
  EXPECT_EQ( ss.str(), "1" );
}

TEST( PhysicalQuantity, conversion )
{
  PQ< NeutralRatio > val{1.};

  EXPECT_NEAR( val.toRatio< Kilo >().get(), 0.001, 0.000001 );
  EXPECT_NEAR( val.toRatio< Milli >().get(), 1000, 0.000001 );

  PQ< Micro > mcopy{val};

  EXPECT_NEAR( mcopy.get(), 1000 * 1000, 0.000001 );

  PQ< Kilo > kcopy{val};

  EXPECT_NEAR( kcopy.get(), 0.001, 0.000001 );
}

int main( int argc, char** argv )
{
  ::testing::InitGoogleTest( &argc, argv );
  return RUN_ALL_TESTS();
}
