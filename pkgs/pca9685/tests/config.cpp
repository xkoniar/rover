
#include "pca9685/config.h"
#include <gtest/gtest.h>

using namespace pca9685;

TEST( PCA9685Config, load )
{
  ros::NodeHandle nh{"~"};

  nh.setParam( "frequency", 60 );
  nh.setParam( "i2c/device", "test" );
  nh.setParam( "i2c/slave", 22 );
  nh.setParam( "channels/test_channel", "test_plugin" );

  PCA9685Config conf = loadStaticConfig( nh );

  ASSERT_EQ( conf.frequency, 60 );
  ASSERT_EQ( conf.i2c.device_path, "test" );
  ASSERT_EQ( conf.i2c.slave_addr, 22 );
  ASSERT_EQ( conf.channels.size(), 1 );
  ASSERT_EQ( conf.channels[0].name, "test_channel" );
  ASSERT_EQ( conf.channels[0].plugin_name, "test_plugin" );
}

int main( int argc, char** argv )
{
  ros::init( argc, argv, "test" );
  ::testing::InitGoogleTest( &argc, argv );
  return RUN_ALL_TESTS();
}
