
#include "pca9685/util.h"
#include <gtest/gtest.h>

using namespace pca9685;

TEST( Util, clamp )
{
  ASSERT_EQ( clamp( 5., 0., 10. ), 5 );
  ASSERT_EQ( clamp( -5., 0., 10. ), 0 );
  ASSERT_EQ( clamp( 15., 0., 10. ), 10 );
  ASSERT_EQ( clamp( 5., -10., 10. ), 5 );
  ASSERT_EQ( clamp( -5., -10., 10. ), -5 );
  ASSERT_EQ( clamp( 15., -10., 10. ), 10 );
  ASSERT_EQ( clamp( -15., -10., 10. ), -10 );
}

TEST( Util, linearInterp )
{
  ASSERT_EQ( linearInterp( 5., 0., 10., 0., 100.), 50 );
  ASSERT_EQ( linearInterp( 50., 0., 100., 0., 10.), 5.);
  ASSERT_EQ( linearInterp( 5., 0., 10., -100., -90.), -95.);
}

int main( int argc, char** argv )
{
  ros::init( argc, argv, "test" );
  ::testing::InitGoogleTest( &argc, argv );
  return RUN_ALL_TESTS();
}
