
#include "pca9685/i2c.h"
#include "gtest/gtest-spi.h"
#include <gtest/gtest.h>

using namespace pca9685;

struct MockIO
{
  static int open( const char* pathname, int flags )
  {
    EXPECT_EQ( flags, O_RDWR );
    EXPECT_NE( pathname, nullptr );
    return 1;
  }
  static int ioctl( int fd, unsigned long request, unsigned int argp )
  {
    EXPECT_EQ( request, I2C_SLAVE );
    EXPECT_EQ( fd, 1 );
    EXPECT_LT( argp, 255 );  // address
    return 1;
  }
  static int dup( int fd )
  {
    EXPECT_EQ( fd, 1 );
    return 2;
  }
  static ssize_t read( int fildes, void* buf, size_t nbyte )
  {
    EXPECT_EQ( fildes, 1 );
    EXPECT_NE( buf, nullptr );
    EXPECT_GT( nbyte, 0 );

    auto* msg = static_cast<uint8_t*>(buf);
    for ( unsigned i = 0; i < nbyte; ++i )
    {
      msg[i] = '*';
    }
    return ssize_t(nbyte);
  }
  static ssize_t write( int fildes, const void* buf, size_t nbyte )
  {
    EXPECT_EQ( fildes, 1 );
    EXPECT_NE( buf, nullptr );
    EXPECT_GT( nbyte, 0 );

    return ssize_t(nbyte);
  }
  static int close( int fd )
  {
    EXPECT_EQ( fd, 1 );
    return 1;
  }
};

TEST( I2C, basic )
{

  I2CCom< MockIO > i2c{"/dev/nope",40};

  ASSERT_FALSE( i2c.setAddress( 0 ) );
  ASSERT_TRUE( i2c.setAddress( 40 ) );

  ASSERT_TRUE( i2c.writeByte( 5, 5 ) );
  ASSERT_TRUE( i2c.readByte( 5 ).success );
}

int main( int argc, char** argv )
{
  ::testing::InitGoogleTest( &argc, argv );
  return RUN_ALL_TESTS();
}
