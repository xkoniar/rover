#include "pca9685/plugins/pwm_channel.h"
#include <pca9685/pca9685_interfaces.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS( pca9685::PWMChannel, pca9685::IChannelHandler )
