/**
 * Copyright 2017 - 2018 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */
#include "pca9685/pca9685.h"
#include "pca9685/util.h"
#include "ros/ros.h"
#include <controller_manager/controller_manager.h>
#include <ros/console.h>

using namespace pca9685;

class Spinner
{
      public:
        Spinner( const ros::NodeHandle& handle, const PCA9685Config& config )
          : loop_timer_(
                handle.createTimer( ros::Duration( 1 / config.frequency ), &Spinner::loop, this, false, false ) )
          , pca9685hw_( config )
          , controller_manager_( &pca9685hw_, handle )
          , last_time( ros::Time::now() )
        {
                loop_timer_.start();
        }

        void loop( const ros::TimerEvent& /*unused*/ )
        {
                ros::Time     actual_time     = ros::Time::now();
                ros::Duration actual_duration = actual_time - last_time;

                pca9685hw_.tick( actual_time, actual_duration );
                controller_manager_.update( actual_time, actual_duration );
                pca9685hw_.write( actual_time, actual_duration );

                last_time = actual_time;
        }

      private:
        ros::Timer                            loop_timer_;
        PCA9685Hardware<>                     pca9685hw_;
        controller_manager::ControllerManager controller_manager_;
        ros::Time                             last_time;
};

int main( int argc, char** argv )
{
        ros::init( argc, argv, "pca9685" );

        ros::NodeHandle node_handle{"~"};

        const PCA9685Config config = loadStaticConfig( node_handle );

        Spinner spinner{node_handle, config};
        
        ros::AsyncSpinner async_spinner{2};
        async_spinner.start();
        ros::waitForShutdown();
}
